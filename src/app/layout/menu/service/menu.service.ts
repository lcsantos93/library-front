import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '@environments/environment';
@Injectable({ providedIn: 'root' })
export class MenuService {
    private header = {
        headers: { 'Accept': 'application/json;'}
    };
    constructor(private http: HttpClient){}
    list = (limit, offset) =>  {
        return this.http.get<any>(`${environment.apiUrl}/api/menu`,  { params: { limit, offset}}).toPromise();
    }
    getAll = () => {
        return this.http.get<any>(`${environment.apiUrl}/api/menu/all`,  { params: { }}).toPromise();
    }
    save = (form) => {
        return this.http.post<any>(`${environment.apiUrl}/api/menu/save`, form, Object.assign({}, this.header))
        .toPromise();
    }
    delete = (id) => {
        return this.http.delete<any>(`${environment.apiUrl}/api/menu/${id}`, {})
        .toPromise();
    }
    edit = (id) => {
        return this.http.get<any>(`${environment.apiUrl}/api/menu/edit/${id}`,  { params: { }}).toPromise();
    }
}