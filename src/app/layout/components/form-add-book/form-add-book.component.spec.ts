import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddBookComponent } from './form-add-book.component';

describe('FormAddBookComponent', () => {
  let component: FormAddBookComponent;
  let fixture: ComponentFixture<FormAddBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAddBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
