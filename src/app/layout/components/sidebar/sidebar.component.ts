import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticationService } from '@app/login/service/authentication.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isActive: boolean;
    collapsed: boolean;
    pushRightClass: string;
    navbar: any = [];
    @Output() collapsedEvent = new EventEmitter<boolean>();

    constructor(public router: Router, private serviceAuth: AuthenticationService) {
        this.navbar = this.serviceAuth.currentUserValue.navbar;
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }
    toogleMenu: boolean = false;
    changePage(nav) {
        if (nav.link != null && nav.link != '') {
            console.log(`/${nav.link}`)
            this.router.navigate([`/${nav.link}`, {}]);
        } else {
            this.toogleMenu = !this.toogleMenu;
        }
    }

    ngOnInit() {
        this.isActive = false;
        this.collapsed = false;
        this.pushRightClass = 'push-right';
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    toggleCollapsed() {}


}
