import { Component, OnInit } from '@angular/core';
import { routerTransition } from '@app/router.animations';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MenuService } from './service/menu.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-menu-edit',
    templateUrl: './menu-edit.component.html',
    styleUrls: ['./menu-edit.component.scss'],
    animations: [routerTransition()]
})
export class MenuEditComponent implements OnInit {
    form: FormGroup;
    isEdit: boolean = false;
    submitted: boolean = false;
    menus: any = [];
    constructor(private fb: FormBuilder, private service: MenuService, private router: Router, private params: ActivatedRoute) {
        let id = this.params.snapshot.paramMap.get("id") || null;
        this.form = this.fb.group({
            men_codigo: new FormControl(id, []),
            men_nome: new FormControl('', [Validators.required]),
            men_ativo: new FormControl(true, [Validators.required]),
            men_men_codigo: new FormControl('', [])
        });
    }

    ngOnInit() {
        let p = [];
        p.push(this.service.getAll());
        if (this.form.controls['men_codigo'].value != null)
            p.push(this.service.edit(this.form.controls['men_codigo'].value))

        Promise.all(p).then(r => {
            this.menus = (r[0].valid) ? r[0].data : [];
            if (typeof r[1] !== 'undefined') {
                if(r[1].valid){
                    let obj =  r[1].data;
                    this.form.controls['men_nome'].setValue(obj.men_nome);
                    this.form.controls['men_ativo'].setValue((obj.men_ativo == '1') ? true : false);
                    this.form.controls['men_men_codigo'].setValue(obj.men_men_codigo || '');
                }
            }
        });
    }

    save() {
        this.submitted = true;
        if (this.form.valid) {
            this.form.value['men_men_codigo'] =  (this.form.value['men_men_codigo'] == '') ? null : this.form.value['men_men_codigo'];
            this.service.save(this.form.value).then((x: any) => {
                if (x.valid)
                    this.router.navigate(['/menu']);
                else
                    alert("Não foi possivel salvar");
            });
        }
    }
}