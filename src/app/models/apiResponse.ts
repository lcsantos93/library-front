export interface ApiResponse { 
    valid?: boolean;
    data?: any;
    message?: string;
}