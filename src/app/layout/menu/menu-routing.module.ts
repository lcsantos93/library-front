import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuListComponent } from './menu-list.component';
import { MenuEditComponent } from './menu-edit.component';

const routes: Routes = [
    {
        path: '',
        component: MenuListComponent
    },
    {
        path: 'add',
        component: MenuEditComponent
    },
    {
        path: 'edit/:id',
        component: MenuEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MenuRoutingModule { }
