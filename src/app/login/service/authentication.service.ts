import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { Auth } from '@app/models/auth';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<Auth>;
    public currentUser: Observable<Auth>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<Auth>(JSON.parse(localStorage.getItem('auth')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): Auth {
        return this.currentUserSubject.value;
    }

    login(user: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/api/authenticate`, { user, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                if(user.valid && user.data.token != null)
                    localStorage.setItem('auth', JSON.stringify(user.data));
                    this.currentUserSubject.next(user.data);
                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('auth');
        this.currentUserSubject.next(null);
    }
}