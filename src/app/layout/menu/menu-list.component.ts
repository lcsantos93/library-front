import { Component, OnInit } from '@angular/core';
import { MenuService } from './service/menu.service';
import { routerTransition } from '@app/router.animations';

@Component({
    selector: 'app-menu-list',
    templateUrl: './menu-list.component.html',
    styleUrls: ['./menu-list.component.scss'],
    animations: [routerTransition()]
})
export class MenuListComponent implements OnInit {
    list: any = [];
    pagination: any = {};
    constructor(private service: MenuService) {

    }

    ngOnInit() {
        this.resetPagination();
        this.search();
    }

    private resetPagination = () => {
        this.pagination = {
            page: 1,
            limit: 10,
            total: 0
        }
    }

    search() {
        this.service.list(this.pagination.limit, ((this.pagination.page - 1) * this.pagination.limit)).then(result => {
            if (result.valid) {
                this.list = result.data;
                this.pagination.total = result.count;
            } else {
                this.list = [];
                this.pagination.total = 0;
            }
        });
    }

    pageChange(page) {
        this.pagination.page = page;
        this.search();
    }

    delete(id) {
        if (window.confirm("Você tem certeza que deseja excluir o registro?")) {
            this.service.delete(id).then(result => {
                if (result.valid) {
                    this.resetPagination();
                    this.search();
                } else {
                    alert('Não foi possivel excluir o registro')
                }
            });
        }
    }
}