import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MenuRoutingModule } from './menu-routing.module';
import {MenuListComponent } from './menu-list.component';
import {MenuEditComponent } from './menu-edit.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StatModule, PageHeaderModule } from '../../shared';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelect2Module } from 'ng-select2';

@NgModule({
    imports: [
        CommonModule,
        MenuRoutingModule,
        StatModule,
        PageHeaderModule,
        JwBootstrapSwitchNg2Module,
        FormsModule,
        ReactiveFormsModule,
        NgSelect2Module,
        NgbModule],
    declarations: [
       MenuListComponent,
       MenuEditComponent
    ]
})
export class MenuModule {}
